# e3-xtpico

e3 wrapper for the xtpico module.

## Installation

```sh
$ make init patch build
$ sudo make install
```

For further targets, type `make`.

## Usage

```sh
$ iocsh.bash -r examplemodule
```

## Contributing

Contributions through pull/merge requests only.
