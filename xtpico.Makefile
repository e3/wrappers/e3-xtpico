#  Copyright (c) 2022    European Spallation Source ERIC
#
#  The program is free software: you can redistribute
#  it and/or modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation, either version 2 of the
#  License, or any newer version.
#
#  This program is distributed in the hope that it will be useful, but WITHOUT
#  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
#  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
#  more details.
#
#  You should have received a copy of the GNU General Public License along with
#  this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt

where_am_I := $(dir $(abspath $(lastword $(MAKEFILE_LIST))))
include $(E3_REQUIRE_TOOLS)/driver.makefile


ifneq ($(strip $(ASYN_DEP_VERSION)),)
  asyn_VERSION=$(ASYN_DEP_VERSION)
endif

EXCLUDE_ARCHS += linux-ppc64e6500
EXCLUDE_ARCHS += linux-corei7-poky

APP     := xtpicoApp
APPDB   := $(APP)/Db
APPSRC  := $(APP)/src

USR_INCLUDES += -I$(where_am_I)$(APPSRC)

HEADERS += $(wildcard $(APPSRC)/*.h)

SOURCES += $(wildcard $(APPSRC)/*.cpp)

DBDS += $(APPSRC)/xtpicoSupport.dbd

SCRIPTS += $(wildcard ../iocsh/*.iocsh)

USR_DBFLAGS += -I . -I ..
USR_DBFLAGS += -I $(EPICS_BASE)/db
USR_DBFLAGS += -I $(APPDB)
USR_DBFLAGS += -I ../templates


TEMPLATES += $(wildcard $(APPDB)/*.db)
TEMPLATES += $(wildcard ../templates/*.db)
TEMPLATES += $(wildcard $(APPDB)/*.template)

SUBS += $(wildcard $(APPDB)/*.substitutions)
SUBS += $(wildcard ../templates/*.substitutions)

TMPS += $(wildcard ../templates/*.template)
